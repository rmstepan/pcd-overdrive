#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "string.h"

// parse the arguments received as whitespace separated string
// parseArgs("test1 test2 test3")
// out:
//    test1
//    test2
//    test3
int parseArgs(char *args){
    int parNum = -1;
    char delim[]=" \n";
    char parCom[10][50];

    char *ptr = strtok(args, delim);

    while (ptr != NULL)
    {
        parNum++;
        strcpy(parCom[parNum], ptr);
        printf("%s \n", ptr);
        ptr = strtok(NULL, delim);
    }
}

void main(int argc, char *argv[]){
    /////////////////////////////////
    // stdin read through pipes
    // exec:
    //   echo <test_string> | ./main

    // stdin read from file
    // exec:
    //   ./main < <test_file>

    // check if stdin is empty
    if(isatty(STDIN_FILENO))
    {
        printf("[-] STDIN empty!\n");
    }
    // move on
    else
    {
        char buffer[1024];
        fgets(buffer, 1024 , stdin);
        printf("[+] STDIN: %s\n", buffer);
        printf("Processing buffer:\n");
        parseArgs(buffer);

    }

    /////////////////////////////////
    // command line arguments read
    // exec: 
    //    ./main <arg1> <arg2> <arg3> ... <argN>
    if(argc == 1) 
        printf("[-] argv empty! \n"); // no arguments provided
    else if(argc >= 2) 
    { 
        for(int counter=0;counter < argc; counter++) 
            printf("[+] argv[%d]: %s\n", counter, argv[counter]); 
    } 
  
}